#!/bin/bash -x

export GMSH_VERSION=$1

if test "$GMSH_VERSION" == ""; then
	echo "please provide the version"
	exit 42
fi

mkdir -p tmp
(cd tmp && svn co --username getdp --password getdp https://geuz.org/svn/getdp/trunk getdp)
pwd
ls tmp
export GMSH_VERSION=$(echo $1~svn$(cd tmp/getdp && svnversion .))
echo "Building getdp_$GMSH_VERSION.orig.tar.gz" 
(cd tmp && mv getdp getdp-$GMSH_VERSION)
(cd tmp && rm -rf getdp-$GMSH_VERSION/contrib/Arpack)
(cd tmp && tar --exclude-vcs -czf getdp_$GMSH_VERSION.orig.tar.gz getdp-$GMSH_VERSION)
(cd tmp && rm -rf getdp-$GMSH_VERSION)
(cd tmp && mv getdp_$GMSH_VERSION.orig.tar.gz ..)
rm -rf tmp
